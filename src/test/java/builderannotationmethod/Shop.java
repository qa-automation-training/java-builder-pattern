package builderannotationmethod;

public class Shop {

	public static void main(String[] args) {
		Phone p = new Phone(6, 4000, 6.5, "Android", "Qualcom", 20);
		System.out.println(p);
		p.builder().age(50).revenue(5000000.59d).build();
	}

}
