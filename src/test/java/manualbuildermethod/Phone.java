package manualbuildermethod;

import lombok.Builder;

public class Phone {

    private int ram;

    private int battery;

    private double screenSize;

    private String os;

    private String processor;

    private int camera;

    public Phone(int ram, int battery, double screenSize, String os, String processor, int camera) {
        super();
        this.ram = ram;
        this.battery = battery;
        this.screenSize = screenSize;
        this.os = os;
        this.processor = processor;
        this.camera = camera;

    }

    public void company(String name, int age, double revenue) {
        System.out.println("name-" + name + " age-" + age + " revenue-" + revenue);
    }
    
    public VoidBuilder builder() {
        return new VoidBuilder();
    }
    
    public class VoidBuilder {
        private String name;
        private int age;
        private double revenue;
        
        public VoidBuilder setName(String name) {
            this.name = name;
            return this;
        
        }
        
        public VoidBuilder setAge(int age) {
            this.age = age;
            return this;
        }
        
        public VoidBuilder setRevenue(double revenue) {
            this.revenue = revenue;
            return this;
        }
        
        public void build() {
            company(name, age, revenue);
        }
        
        
    }

}
