package builderannotationconstructor;

import lombok.Builder;
import lombok.ToString;

@ToString
public class Phone {
    
    private int ram;
    private int battery;
    private double screenSize;
    private String os;
    private String processor;
    private int camera;
    
    @Builder
    public Phone(int ram, int battery, String os, String processor) {
        super();
        this.ram = ram;
        this.battery = battery;
        this.os = os;
        this.processor = processor;

    }

}
