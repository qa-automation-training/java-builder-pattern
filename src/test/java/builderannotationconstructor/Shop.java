package builderannotationconstructor;

public class Shop {

	public static void main(String args[]) {
		System.out.println(Phone.builder().os("Android").ram(8).battery(5000).build());
	}

}
