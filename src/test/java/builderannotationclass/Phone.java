package builderannotationclass;

import lombok.Builder;
import lombok.ToString;

@Builder
@ToString
public class Phone {
    
    private int ram;
    private int battery;
    private double screenSize;
    private String os;
    private String processor;
    private int camera;

}
