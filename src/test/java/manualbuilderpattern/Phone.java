package manualbuilderpattern;

public class Phone {
    
    private int ram;
    private int battery;
    private double screenSize;
    private String os;
    private String processor;
    private int camera;
    
    public Phone(int ram, int battery, double screenSize, String os, String processor, int camera) {
        super();
        this.ram = ram;
        this.battery = battery;
        this.screenSize = screenSize;
        this.os = os;
        this.processor = processor;
        this.camera = camera;
 }
    
    public static PhoneBuilder builder() {
        return new PhoneBuilder();
    }
    
    
    //@ToString
    public static class PhoneBuilder{
        private int ram;
        private int battery;
        private double screenSize;
        private String os;
        private String processor;
        private int camera;
        
        public PhoneBuilder setRam(int ram) {
            this.ram = ram;
            return this;
        
        }
        
        public PhoneBuilder setBattery(int battery) {
            this.battery = battery;
            return this;
        
        }
        
        public PhoneBuilder setScreenSize(double screenSize) {
            this.screenSize = screenSize;
            return this;
        
        }
        
        public PhoneBuilder setOs(String os) {
            this.os = os;
            return this;
        
        }
        
        public PhoneBuilder setProcessor(String processor) {
            this.processor = processor;
            return this;
        
        }
        
        public PhoneBuilder setCamera(int camera) {
            this.camera = camera;
            return this;
        
        }   
        
        public Phone build() {
            return new Phone(ram, battery, screenSize, os, processor, camera);
        }

        @Override
        public String toString() {
            return "phoneBuilder [ram=" + ram + ", battery=" + battery + ", screenSize=" + screenSize + ", os=" + os
                + ", processor=" + processor + ", camera=" + camera + "]";

        }
        
        
        
    }
}
