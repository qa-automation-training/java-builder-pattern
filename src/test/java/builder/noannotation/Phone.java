package builder.noannotation;

public class Phone {

    private int ram;
    private int battery;
    private double screenSize;
    private String os;
    private String processor;
    private int camera;
    
    public Phone(int ram, int battery, double screenSize, String os, String processor, int camera) {
        super();
        this.ram = ram;
        this.battery = battery;
        this.screenSize = screenSize;
        this.os = os;
        this.processor = processor;
        this.camera = camera;
 }
    
    @Override
    public String toString() {
        return "Phone [ram=" + ram + ", battery=" + battery + ", screenSize=" + screenSize + ", os=" + os
            + ", processor=" + processor + ", camera=" + camera + "]";

    }
    
    
}

