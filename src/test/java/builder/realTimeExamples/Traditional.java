package builder.realTimeExamples;

import io.restassured.RestAssured;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
@RequiredArgsConstructor
@Data
public class Traditional {

	private int ram;

	private int battery;

	private double screenSize;

	@NonNull
	private String os;

	private String processor;

	private int camera;

	public static void main(String[] args) {
		RestAssured.baseURI = "http://localhost:80";
		Traditional traditional = new Traditional("traditional");
		traditional.setBattery(12);
		traditional.setCamera(36);
		traditional.setRam(1254);
		traditional.setScreenSize(21);
		traditional.setProcessor("sdf");
		RestAssured.given().header("skdkf", "sdfdsf").body(traditional).log().all().when().post();
	}

}
