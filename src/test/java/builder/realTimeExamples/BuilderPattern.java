package builder.realTimeExamples;

import io.restassured.RestAssured;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Builder
@Setter
@Getter
public class BuilderPattern {

	private int ram;

	private int battery;

	private double screenSize;

	private String os;

	private String processor;

	private int camera;

	public static void main(String[] args) {
		RestAssured.baseURI = "http://localhost:80";
		RestAssured.given().header("skdkf", "sdfdsf").body(BuilderPattern.builder().screenSize(1212).os("os")
				.processor("sdf").ram(45).battery(45).camera(12).build()).log().all().when().post();
	}

}
